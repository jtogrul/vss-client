package support;

import com.sun.javafx.application.PlatformImpl;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import it.sauronsoftware.junique.MessageHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.paint.Color;
import javafx.stage.*;
import jupar.Downloader;
import jupar.Updater;
import jupar.objects.Modes;
import jupar.objects.Release;
import jupar.parsers.ReleaseXMLParser;
import org.xml.sax.SAXException;
import support.model.Message;
import support.service.HelpService;
import support.service.event.LoginEvent;
import support.service.event.NewMessagesEvent;
import support.service.listener.LoginListener;
import support.service.listener.NewMessageListener;
import support.ui.StageManager;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import static support.config.Config.*;

public class Main extends Application {

    HelpService helpService;
    StageManager stageManager;
    Preferences preferences;

    Stage primaryStage;

    TrayIcon trayIcon;
    SystemTray tray;

    String appId;

    private boolean manuallyOpened = true;

    static {
        javafx.scene.text.Font.loadFont(Main.class.getResource("/fonts/fontawesome-webfont.ttf").toExternalForm(), 10);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {

        // Use system proxies
        System.setProperty("java.net.useSystemProxies", "true");

        Parameters parameters = getParameters();
        List<String> args = parameters.getRaw();

        // Run only single instance

        appId = Main.class.getPackage().toString();
        boolean alreadyRunning;

        try {
            JUnique.acquireLock(appId, new MessageHandler() {
                public String handle(String message) {

                    primaryStage.show();
                    return null;
                }
            });
            alreadyRunning = false;
        } catch (AlreadyLockedException e) {
            alreadyRunning = true;
        }

        System.out.println(alreadyRunning);

        if (alreadyRunning) {

            for (int i = 0; i < args.size(); i++) {
                System.err.println("Other instance of this app already exist. Closing this, and opening the other one");
                JUnique.sendMessage(appId, "open");
                com.sun.javafx.application.PlatformImpl.tkExit();
                Platform.exit();
                System.exit(1);
            }
        } else {

            if (args.contains(CONSOLE_HIDDEN_ARGUMENT)) {
                manuallyOpened = false;
            }

            this.primaryStage = primaryStage;

            startApp();
        }

    }


    public static void main(String[] args) {
        launch(args);
    }

    public void startApp() {
        addAppToTray();

        checkForNewVersion();

        helpService = HelpService.getInstance();
        stageManager = new StageManager();
        preferences = Preferences.systemNodeForPackage(Main.class);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                event.consume();
                System.out.println("hide");
                showDummyStage();
                primaryStage.hide();
            }
        });

        helpService.addOnLoginListener(new LoginListener() {
            @Override
            public void onLogin(LoginEvent event) {

                System.out.println("LOGGED IN");

                if (manuallyOpened) {
                    stageManager.openMainStage(primaryStage);
                }

            }
        });

        helpService.addOnNewMessageListener(new NewMessageListener() {
            @Override
            public void onNewMessages(NewMessagesEvent event) {

                System.out.println("NEW MESSAGES");

                final List<Message> messages = event.getMessages();

                if (null != trayIcon) {

                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            trayIcon.displayMessage(
                                    "VSS Texniki Dəstək",
                                    "Sizin " + messages.size() + " oxunmamış mesajınız var",
                                    java.awt.TrayIcon.MessageType.INFO
                            );

                        }
                    });
                }
            }
        });

        if (prefsExist()) {
            String username = preferences.get(PREF_NAME_USER, "");
            String password = preferences.get(PREF_NAME_PASS, "");

            try {
                boolean success = helpService.checkAuth(username, password);

                if (!success) {
                    preferences.remove(PREF_NAME_USER);
                    preferences.remove(PREF_NAME_PASS);
                    preferences.flush();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            stageManager.openLoginStage();
        }
    }

    /**
     * Sets up a system tray icon for the application.
     */
    private void addAppToTray() {
        try {
            // ensure awt toolkit is initialized.
            Toolkit.getDefaultToolkit();

            // app requires system tray support, just exit if there is no support.
            if (!SystemTray.isSupported()) {
                System.out.println("No system tray support, application exiting.");
                Platform.exit();
            }

            tray = SystemTray.getSystemTray();

            Image image = ImageIO.read(getClass().getResource("/icons/icon20.png"));
            trayIcon = new TrayIcon(image);

            trayIcon.setToolTip("VSS Support");

            MenuItem openItem = new MenuItem("Texniki destek");
            //openItem.addActionListener(getShowMainStageActionListener());
            openItem.addActionListener(new ReopenActionListener());

            Font defaultFont = Font.decode(null);
            Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
            openItem.setFont(boldFont);


            MenuItem exitItem = new MenuItem("Chixish");
            exitItem.addActionListener(new ExitActionListener());

            // Tray icon right click popup menu
            final PopupMenu popup = new PopupMenu();
            popup.add(openItem);
            popup.addSeparator();
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);

            trayIcon.addActionListener(new ReopenActionListener());

            tray.add(trayIcon);
        } catch (Exception e) {
            System.out.println("Unable to init system tray");
            e.printStackTrace();
        }
    }

    private boolean prefsExist() {
        return preferences.get(PREF_NAME_USER, null) != null && preferences.get(PREF_NAME_PASS, null) != null;
    }

    private void checkForNewVersion() {

        System.out.println("Checking for new version");

        /**
         * Check for new version
         */
        int answer = -1;
        Release release = new Release();
        release.setpkgver("1.0");
        release.setPkgrel("2");
        ReleaseXMLParser parser = new ReleaseXMLParser();
        try {
            Release current =
                    parser.parse("http://vssgroup.ws/soft/latest.xml", Modes.URL);
            if (current.compareTo(release) > 0) {

                System.out.println("new version available");

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Update Confirmation");
                alert.setHeaderText("Yeni versiya mövcuddur");
                alert.setContentText("Bu proqramı ən son versiyaya yeniləmək istəyirsiniz?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    // ... user chose OK
                    Downloader dl = new Downloader();
                    dl.download("http://vssgroup.ws/soft/files.xml", "tmp", Modes.URL);
                    answer = 0;
                } else {
                    // ... user chose CANCEL or closed the dialog
                }


            }
        } catch (SAXException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Something went wrong!");
            alert.setContentText("The xml wasn't loaded succesfully!");
            alert.showAndWait();
//            JOptionPane.showMessageDialog(rootPane, "The xml wasn't loaded succesfully!\n",
//                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            answer = -1;
        } catch (FileNotFoundException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Something went wrong!");
            alert.setContentText("Files were unable to be read or created successfully!\n"
                    + "Please be sure that you have the right permissions and"
                    + " internet connectivity!");
            alert.showAndWait();
//            JOptionPane.showMessageDialog(rootPane,
//                    "Files were unable to be read or created successfully!\n"
//                            + "Please be sure that you have the right permissions and"
//                            + " internet connectivity!",
//                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            answer = -1;
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Something went wrong!");
            alert.setContentText("IOEXception!");
            alert.showAndWait();
//            JOptionPane.showMessageDialog(rootPane, "IOEXception!",
//                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            answer = -1;
        } catch (InterruptedException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Something went wrong!");
            alert.setContentText("The connection has been lost!\n"
                            + "Please check your internet connectivity!");
            alert.showAndWait();
//            JOptionPane.showMessageDialog(rootPane, "The connection has been lost!\n"
//                            + "Please check your internet connectivity!",
//                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            answer = -1;
        }

/**
 * Start the updating procedure
 */
        if (answer == 0) {

            System.out.println("Updating");

            try {
                Updater update = new Updater();
                update.update("update.xml", "tmp", Modes.FILE);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Update");
                alert.setHeaderText("Update successful");
                alert.setContentText("The update was completed successfuly.\n"
                        + "Please restart the application in order the changes take effect.");
                alert.showAndWait();

                // exit
                tray.remove(trayIcon);
                PlatformImpl.tkExit();
                Platform.exit();
                System.exit(1);

            } catch (SAXException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

/**
 * Delete tmp directory
 */
        File tmp = new File("tmp");
        if (tmp.exists()) {
            for (File file : tmp.listFiles()) {
                file.delete();
            }
            tmp.delete();
        }

    }

    private void terminate() {
        helpService.stopCheckingNewMessages();
        tray.remove(trayIcon);

        // clear login data
        preferences.remove(PREF_NAME_USER);
        preferences.remove(PREF_NAME_PASS);

        com.sun.javafx.application.PlatformImpl.tkExit();
        Platform.exit();
        System.exit(1);
    }

    private void showDummyStage() {
        Stage dummyPopup = new Stage();
        dummyPopup.initModality(Modality.NONE);
        // set as utility so no iconification occurs
        dummyPopup.initStyle(StageStyle.UTILITY);
        // set opacity so the window cannot be seen
        dummyPopup.setOpacity(0d);
        // not necessary, but this will move the dummy stage off the screen
        final Screen screen = Screen.getPrimary();
        final Rectangle2D bounds = screen.getVisualBounds();
        dummyPopup.setX(bounds.getMaxX());
        dummyPopup.setY(bounds.getMaxY());
        // create/add a transparent scene
        final Group root = new Group();
        dummyPopup.setScene(new Scene(root, 1d, 1d, Color.TRANSPARENT));
        // show the dummy stage
        dummyPopup.show();
    }

    private class ExitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            terminate();
        }
    }

    private class ReopenActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            System.out.println("open main stage");

            Platform.runLater(new Runnable() {
                @Override
                public void run() {

                    if (! primaryStage.isShowing()) {
                        System.out.println("not showing. show now");
                        stageManager.openMainStage(primaryStage);
                    }
                }
            });
        }
    }
}
