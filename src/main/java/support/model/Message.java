package support.model;

/**
 * Created by togrul on 1/30/15.
 */
public class Message {

    private String id;
    private String body;
    private String role;
    private String fullName;
    private boolean read;
    private Ticket ticket;
    private String created;

    public Message() {

    }

    public Message(String body, Ticket ticket) {
        this.setBody(body);
        this.setTicket(ticket);
    }

    @Override
    public boolean equals(Object obj) {

        Message other = (Message) obj;

        if (null == this.id || null == other || null == other.getId()) {
            return false;
        } else {
            return this.id.equals(other.getId());
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
