package support.model;

/**
 * Created by togrul on 2/1/15.
 */
public class ApiResponse {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isOK() {
        return "ok".equals(this.status);
    }

    @Override
    public String toString() {
        return String.format("Api Response { status: %s, message: %s }", this.status, this.getMessage());
    }
}
