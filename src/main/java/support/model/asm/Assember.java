package support.model.asm;

import support.model.*;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by togrul on 1/30/15.
 */
public class Assember {

    /***
     * {
     "id": "2",
     "customer_id": "3",
     "equipment_id": "2",
     "status_id": "4",
     "request_type_id": "1",
     "subject": "Test subject 2",
     "viewed": false,
     "open": true,
     "satisfied": false,
     "created": "2014-09-27 22:18:01",
     "modified": "2014-10-29 11:52:49"
     }
     * @param obj
     * @return
     */
    public static Ticket buildTicket(JSONObject obj) throws JSONException {

        Ticket ticket = new Ticket();
        ticket.setId(obj.getString("id"));
        ticket.setSubject(obj.getString("subject"));
        ticket.setViewed(obj.getBoolean("viewed"));
        if (!obj.isNull("satisfied")) {
            ticket.setSatisfied(obj.getBoolean("satisfied"));
        }
        ticket.setOpen(obj.getBoolean("open"));
        ticket.setCreated(obj.getString("created"));
        return ticket;
    }

    public static Ticket buildTicketDetailed(JSONObject obj) throws JSONException {

        Ticket ticket = buildTicket(obj.getJSONObject("Ticket"));
        Person customer = buildPerson(obj.getJSONObject("Customer"));
        Equipment equipment = buildEquipment(obj.getJSONObject("Equipment"));
        Status status = buildStatus(obj.getJSONObject("Status"));
        RequestType requestType = buildRequestType(obj.getJSONObject("RequestType"));
        ticket.setCustomer(customer);
        ticket.setEquipment(equipment);
        ticket.setStatus(status);
        ticket.setRequestType(requestType);

        if (obj.has("Message")) {

            JSONArray messageArray = obj.getJSONArray("Message");
            List<Message> messages = buildMessageList(messageArray);
            ticket.setMessages(messages);
        }

        return ticket;
    }

    /**
     * {
     "id": "13",
     "ticket_id": "2",
     "user_id": "2",
     "full_name": "",
     "role": "customer",
     "body": "test message dmsfnsjfn",
     "read": true,
     "created": "2014-10-27 22:18:01",
     "modified": "2014-10-27 22:18:01"
     }
     * @param obj
     * @return
     */
    public static Message buildMessage(JSONObject obj) throws JSONException {
        Message message = new Message();
        message.setId(obj.getString("id"));
        message.setFullName(obj.getString("full_name"));
        message.setRole(obj.getString("role"));
        message.setBody(obj.getString("body"));
        message.setRead(obj.getBoolean("read"));
        message.setCreated(obj.getString("created"));
        return message;
    }

    public static Message buildMessageDetailed(JSONObject obj) throws JSONException {

        Message message = buildMessage(obj.getJSONObject("Message"));
        Ticket ticket = buildTicket(obj.getJSONObject("Ticket"));
        message.setTicket(ticket);
        return message;
    }

    /**
     * {
     "id": "3",
     "user_id": "2",
     "company_id": "1",
     "first_name": "Example",
     "last_name": "Customer",
     "position": "",
     "room": "",
     "phone": "",
     "email": "",
     "created": "2014-10-17 17:39:30",
     "modified": "2014-10-17 17:39:30",
     "full_name": "Example Customer"
     }
     * @param obj
     * @return
     */
    public static Person buildPerson(JSONObject obj) throws JSONException {
        Person person = new Person();
        if (obj.has("id")) {
            person.setId(obj.getString("id"));
        }
        person.setFullName(obj.getString("full_name"));
        person.setPosition(obj.getString("position"));
        //person.setRole(obj.getString("role"));
        return person;
    }

    public static Status buildStatus(JSONObject obj) throws JSONException {
        Status status = new Status();
        status.setId(obj.getString("id"));
        status.setName("null" == obj.getString("name") ? "Növbədə" : obj.getString("name"));
        return status;
    }

    public static RequestType buildRequestType(JSONObject obj) throws JSONException {
        RequestType requestType = new RequestType();
        requestType.setId(obj.getString("id"));
        requestType.setName(obj.getString("name"));
        return requestType;
    }

    public static Equipment buildEquipment(JSONObject obj) throws JSONException {
        Equipment equipment = new Equipment();
        equipment.setId(obj.getString("id"));
        equipment.setName(obj.getString("name"));
        return  equipment;
    }

    public static ApiResponse buildApiResponse(JSONObject obj) throws JSONException {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(obj.getString("status"));
        apiResponse.setMessage(obj.getString("message"));
        return  apiResponse;
    }

    public static List<Ticket> buildTicketListDetailed(JSONArray arr) throws JSONException {
        List<Ticket> ticketList = new ArrayList<Ticket>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            ticketList.add(buildTicketDetailed(obj));
        }
        return ticketList;
    }

    public static List<Message> buildMessageList(JSONArray arr) throws JSONException {
        List<Message> messageList = new ArrayList<Message>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            messageList.add(buildMessage(obj));
        }
        return messageList;
    }

    public static List<Message> buildMessageListDetailed(JSONArray arr) throws JSONException {
        List<Message> messageList = new ArrayList<Message>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            messageList.add(buildMessageDetailed(obj));
        }
        return messageList;
    }

    public static List<RequestType> buildRequestTypeList(JSONArray arr) throws JSONException {
        List<RequestType> requestTypeList = new ArrayList<RequestType>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i).getJSONObject("RequestType");
            requestTypeList.add(buildRequestType(obj));
        }
        return requestTypeList;
    }

    public static List<Equipment> buildEquipmentList(JSONArray arr) throws JSONException {
        List<Equipment> equipmentList = new ArrayList<Equipment>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i).getJSONObject("Equipment");
            equipmentList.add(buildEquipment(obj));
        }
        return equipmentList;
    }

}
