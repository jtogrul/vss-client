package support.service.event;

import us.monoid.web.JSONResource;

/**
 * Created by togrul on 1/30/15.
 */
public class LoginEvent {

    private JSONResource customerJson;

    public LoginEvent(JSONResource customerJson) {
        this.customerJson = customerJson;
    }

    public JSONResource getCustomerJson() {
        return customerJson;
    }
}
