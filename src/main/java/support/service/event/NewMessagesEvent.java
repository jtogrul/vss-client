package support.service.event;

import support.model.Message;
import java.util.List;

/**
 * Created by togrul on 1/30/15.
 */
public class NewMessagesEvent {
    private List<Message> messages;

    public NewMessagesEvent(List<Message> messages) {
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }
}
