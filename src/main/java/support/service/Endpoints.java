package support.service;

/**
 * Created by togrul on 1/30/15.
 */
public class Endpoints {

    public static final String CHECK_LOGIN = "check_login.json";
    public static final String TICKETS = "tickets.json";
    public static final String TICKET_BY_ID = "ticket/%s.json";
    public static final String MESSAGES_UNREAD = "messages_unread.json";
    public static final String REQUEST_TYPES = "request_types.json";
    public static final String EQUIPMENTS = "equipments.json";
    public static final String TICKET_ADD = "ticket_add.json";
    public static final String MESSAGE_ADD = "message_add.json";
    public static final String TICKET_SET_SATISFACTION = "ticket_set_satisfaction.json";
    public static final String TICKET_SET_OPEN = "ticket_set_open.json";
}
