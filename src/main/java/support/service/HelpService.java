package support.service;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import support.config.Config;
import support.model.*;
import support.model.asm.Assember;
import support.service.event.LoginEvent;
import support.service.event.NewMessagesEvent;
import support.service.listener.LoginListener;
import support.service.listener.NewMessageListener;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.TextResource;

import java.util.*;
import java.util.function.Predicate;

import static support.service.Endpoints.*;
import static us.monoid.web.Resty.data;
import static us.monoid.web.Resty.form;

/**
 * Created by togrul on 1/30/15.
 */
public class HelpService {

    private static HelpService ourInstance = new HelpService();
    public static HelpService getInstance() {
        return ourInstance;
    }

    private Resty resty = new Resty();
    private List<String> unreadMessageIds;
    private List<LoginListener> loginListeners = new ArrayList<LoginListener>();
    private List<NewMessageListener> newMessageListeners = new ArrayList<NewMessageListener>();
    private boolean authenticated = false;

    public Person user;

    Timer messageCheckTimer;

    private ObservableList<Message> unreadMessagesObservableList = FXCollections.observableArrayList();

    private HelpService() {
        unreadMessageIds = new ArrayList<String>();
    }

    public ObservableList<Message> getUnreadMessagesObservableList() {
        return unreadMessagesObservableList;
    }

    public void startCheckingNewMessages() {

        messageCheckTimer = new Timer();
        TimerTask checkMessagesTask = new TimerTask() {
            @Override
            public void run() {

                System.out.println("checked online");

                try {

                    final List<Message> unreadMessages = getUnreadMessages();
                    final List<Message> newUnreadMessages = new ArrayList<Message>();

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            for (Message message : unreadMessages) {

                                if (! unreadMessagesObservableList.contains(message)) {
                                    newUnreadMessages.add(message);
                                }
                            }

                            unreadMessagesObservableList.removeIf(new Predicate<Message>() {
                                @Override
                                public boolean test(Message message) {
                                    return !unreadMessages.contains(message);
                                }
                            });


                            unreadMessagesObservableList.addAll(newUnreadMessages);

                            if (! newUnreadMessages.isEmpty()) {
                                fireNewMessagesEvent(new NewMessagesEvent(newUnreadMessages));
                            }
                        }
                    });



                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        messageCheckTimer.scheduleAtFixedRate(checkMessagesTask, 500, 1000);
    }

    public void stopCheckingNewMessages() {
        messageCheckTimer.cancel();
    }

    public boolean checkAuth(String authUser, String authPass) {

        try {
            resty.authenticate(Config.ROOT, authUser, authPass.toCharArray());

            JSONResource response = resty.json(path(CHECK_LOGIN));

            String status = (String) response.get("status");

            if("ok".equals(status)) {
                this.authenticated = true;

                this.user = Assember.buildPerson(response.toObject().getJSONObject("customer").getJSONObject("Customer"));

                fireLoginEvent(new LoginEvent(response));

                startCheckingNewMessages();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return authenticated;
    }

    public List<Ticket> getTickets() throws Exception {

        JSONResource response = resty.json(path(TICKETS));

        List<Ticket> ticketList = Assember.buildTicketListDetailed(response.toObject().getJSONArray("tickets"));

        return ticketList;

    }

    public Ticket getTicket(String id) throws Exception {

        JSONResource response = resty.json(path(TICKET_BY_ID, id));

        Ticket ticket = null;

        if("ok".equals(response.get("status"))) {

            ticket = Assember.buildTicketDetailed(response.toObject().getJSONObject("ticket"));
        } else {
            // TODO not found exception
        }

        return ticket;
    }

    private List<Message> getUnreadMessages() throws Exception {

        JSONResource response = resty.json(path(MESSAGES_UNREAD));

        List<Message> messageList = null;

        messageList = Assember.buildMessageListDetailed(response.toObject().getJSONArray("messages"));

        return messageList;
    }

    public ApiResponse addMessage(Message message) throws Exception {

        ApiResponse apiResponse = null;

        JSONResource response = resty.json(path(MESSAGE_ADD), form(
                data("data[Message][ticket_id]", message.getTicket().getId()),
                data("data[Message][body]", message.getBody())
        ));

        apiResponse = Assember.buildApiResponse(response.toObject());

        return apiResponse;
    }

    public ApiResponse addTicket(Ticket ticket) throws Exception {

        ApiResponse apiResponse = null;

        JSONResource response = resty.json(path(TICKET_ADD), form(
                data("data[Ticket][subject]", ticket.getSubject()),
                data("data[Ticket][message]", ticket.getFirstMessage()),
                data("data[Ticket][equipment_id]", ticket.getEquipment().getId()),
                data("data[Ticket][request_type_id]", ticket.getRequestType().getId())
        ));

        apiResponse = Assember.buildApiResponse(response.toObject());

        return apiResponse;
    }

    public ApiResponse setOpen(Ticket ticket, boolean open) throws Exception {

        ApiResponse apiResponse = null;

        JSONResource response = resty.json(path(TICKET_SET_OPEN), form(
                data("data[Ticket][id]", ticket.getId()),
                data("data[Ticket][open]", open ? "1" : "0"))
        );

        apiResponse = Assember.buildApiResponse(response.toObject());

        return apiResponse;
    }

    public ApiResponse setSatisfaction(Ticket ticket, boolean satisfaction) throws Exception {

        ApiResponse apiResponse = null;

        JSONResource response = resty.json(path(TICKET_SET_SATISFACTION), form(
                        data("data[Ticket][id]", ticket.getId()),
                        data("data[Ticket][satisfied]", satisfaction ? "1": "0"))
        );

        apiResponse = Assember.buildApiResponse(response.toObject());

        return apiResponse;
    }

    public List<RequestType> getRequestTypeList() throws Exception {

        JSONResource response = resty.json(path(REQUEST_TYPES));
        List<RequestType> requestTypeList = null;
        requestTypeList = Assember.buildRequestTypeList(response.toObject().getJSONArray("requestTypes"));
        return requestTypeList;
    }

    public List<Equipment> getEquipmentList() throws Exception {

        JSONResource response = resty.json(path(EQUIPMENTS));
        List<Equipment> equipmentList = null;
        equipmentList = Assember.buildEquipmentList(response.toObject().getJSONArray("equipments"));
        return equipmentList;
    }

    public void addOnLoginListener(LoginListener loginListener) {
        loginListeners.add(loginListener);
    }

    public void addOnNewMessageListener(NewMessageListener newMessageListener) {
        newMessageListeners.add(newMessageListener);
    }

    private void fireLoginEvent(LoginEvent loginEvent) {

        Iterator<LoginListener> iterator = loginListeners.iterator();

        while (iterator.hasNext()) {
            LoginListener listener = iterator.next();

            if (null != listener) {
                listener.onLogin(loginEvent);
            }
        }
    }

    private void fireNewMessagesEvent(NewMessagesEvent newMessagesEvent) {
        for (NewMessageListener listener : newMessageListeners) {
            if (null != listener) {
                listener.onNewMessages(newMessagesEvent);
            }
        }
    }

    private String path(String path, String... params) {
        return Config.ROOT + String.format(path, params);
    }

}
