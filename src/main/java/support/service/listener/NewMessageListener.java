package support.service.listener;

import support.service.event.NewMessagesEvent;

/**
 * Created by togrul on 1/30/15.
 */
public interface NewMessageListener {
    public void onNewMessages(NewMessagesEvent event);
}
