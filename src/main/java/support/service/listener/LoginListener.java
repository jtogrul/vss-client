package support.service.listener;

import support.service.event.LoginEvent;

/**
 * Created by togrul on 1/30/15.
 */
public interface LoginListener {
    public void onLogin(LoginEvent event);
}
