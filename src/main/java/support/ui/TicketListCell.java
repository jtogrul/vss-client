package support.ui;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import support.model.Ticket;

/**
 * Created by togrul on 1/31/15.
 */
public class TicketListCell extends ListCell<Ticket> {

    private static final String CACHE_LIST_NAME_CLASS = "cache-list-name";
    private static final String CACHE_LIST_DT_CLASS = "cache-list-dt";

    private GridPane grid = new GridPane();
    private Label name = new Label();
    private Label dt = new Label();

    public TicketListCell() {
        configureGrid();
        configureName();
        configureDifficultyTerrain();
        addControlsToGrid();
    }

    private void configureGrid() {
        grid.setHgap(10);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 10, 0, 10));
    }

    private void configureName() {
        name.getStyleClass().add(CACHE_LIST_NAME_CLASS);
    }

    private void configureDifficultyTerrain() {
        dt.getStyleClass().add(CACHE_LIST_DT_CLASS);
    }

    private void addControlsToGrid() {
        grid.add(name, 0, 0);
        grid.add(dt, 0, 1);
    }

    @Override
    public void updateItem(Ticket ticket, boolean empty) {
        super.updateItem(ticket, empty);
        if (empty) {
            clearContent();
        } else {
            addContent(ticket);
        }
    }

    private void clearContent() {
        setText(null);
        setGraphic(null);
    }

    private void addContent(Ticket ticket) {
        setText(null);
        name.setText("#" + ticket.getId() + ": " + ticket.getSubject());
        name.setFont(Font.font(null, FontWeight.BOLD, 20));
        dt.setText("Yaradılma tarixi: " + ticket.getCreated() + ", Status: " + ticket.getStatus().getName());
        setGraphic(grid);
    }

}
