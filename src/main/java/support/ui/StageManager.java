package support.ui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import support.controllers.TicketController;
import support.model.Ticket;

import java.io.IOException;

/**
 * Created by togrul on 2/1/15.
 */
public class StageManager {

    public void openMainStage(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setTitle("VSS Support");
        Scene scene = new Scene(root, 300, 275);
        scene.getStylesheets().add("style/style.css");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public void openNewTicketStage() {

        Stage stage = new Stage();
        stage.setTitle("Yeni sorğu");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/new.fxml"));

        StackPane myPane = null;
        try {
            myPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(myPane);
        scene.getStylesheets().add("style/style.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
    
    public void openTicketStage(Ticket ticket) {

        Stage stage = new Stage();
        stage.setTitle("Sorğu #" + ticket.getId());
        VBox myPane = null;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ticket.fxml"));
        try {
            myPane = loader.load();

            TicketController ticketController = loader.getController();
            ticketController.setTicket(ticket);

        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(myPane);
        scene.getStylesheets().add("style/style.css");
        stage.setScene(scene);

        stage.show();
    }

    public void openLoginStage() {

        Stage stage = new Stage();
        stage.setTitle("Giriş");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));

        StackPane myPane = null;
        try {
            myPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(myPane);
        scene.getStylesheets().add("style/style.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public void openMyTicketsStage() {
        Stage stage = new Stage();
        stage.setTitle("Mənim sorğularım");
        VBox myPane = null;
        try {
            myPane = FXMLLoader.load(getClass().getResource("/tickets.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(myPane);
        scene.getStylesheets().add("style/style.css");
        stage.setScene(scene);

        stage.show();
    }
}
