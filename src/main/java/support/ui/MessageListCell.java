package support.ui;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import support.model.Message;

/**
 * Created by togrul on 1/31/15.
 */
public class MessageListCell extends ListCell<Message> {

    private static final String ICON_SUPPORT = "\uf1cd";
    private static final String ICON_CUSTOMER = "\uf007";
    private static final String FONT_AWESOME = "FontAwesome";

    private GridPane grid = new GridPane();
    private Label icon = new Label();
    private Label name = new Label();
    private Label dt = new Label();

    public MessageListCell() {
        configureGrid();
        configureIcon();
        configureName();
        configureBody();
        addControlsToGrid();
    }

    private void configureGrid() {
        grid.setHgap(10);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 10, 0, 10));
    }

    private void configureIcon() {
        icon.setFont(Font.font(FONT_AWESOME, FontWeight.BOLD, 24));
    }

    private void configureName() {
        //name.getStyleClass().add(CACHE_LIST_NAME_CLASS);
    }

    private void configureBody() {
        //dt.getStyleClass().add(CACHE_LIST_DT_CLASS);
    }

    private void addControlsToGrid() {
        grid.add(icon, 0, 0, 1, 2);
        grid.add(name, 1, 0);
        grid.add(dt, 1, 1);
    }

    @Override
    public void updateItem(Message message, boolean empty) {
        super.updateItem(message, empty);
        if (empty) {
            clearContent();
        } else {
            addContent(message);
        }
    }

    private void clearContent() {
        setText(null);
        setGraphic(null);
    }

    private void addContent(Message message) {
        setText(null);
        icon.setText("test");
        name.setText(message.getFullName());
        dt.setText(message.getBody());
        setIconDependingOnRole(message);
        setGraphic(grid);
    }

    private void setIconDependingOnRole(Message message) {

        if ("customer".equals(message.getRole())) {
            icon.setText(ICON_CUSTOMER);
        } else {
            icon.setText(ICON_SUPPORT);
        }

    }
}
