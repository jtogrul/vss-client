package support.ui;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import support.model.Message;

/**
 * Created by togrul on 2/1/15.
 */
public class UnreadMessageListCell extends ListCell<Message> {

    private GridPane grid = new GridPane();
    private Label name = new Label();
    private Label dt = new Label();

    public UnreadMessageListCell() {
        configureGrid();
        addControlsToGrid();
    }

    private void configureGrid() {
        grid.setHgap(10);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 10, 0, 10));
    }

    private void addControlsToGrid() {
        grid.add(name, 1, 0);
        grid.add(dt, 1, 1);
    }

    @Override
    public void updateItem(Message message, boolean empty) {
        super.updateItem(message, empty);
        if (empty) {
            clearContent();
        } else {
            addContent(message);
        }
    }

    private void clearContent() {
        setText(null);
        setGraphic(null);
    }

    private void addContent(Message message) {
        setText(null);
        name.setText(message.getBody());
        dt.setText(message.getTicket().getSubject());
        setGraphic(grid);
    }
}
