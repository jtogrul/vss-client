package support.config;

/**
 * Created by togrul on 1/30/15.
 */
public class Config {

    public static final String ROOT = "http://vssgroup.ws/help/api/";
    public static final long REFRESH_INTERVAL = 1000L;

    public static final String PREF_NAME_USER = "USERNAME";
    public static final String PREF_NAME_PASS = "PASSWORD";

    public static final String CONSOLE_HIDDEN_ARGUMENT = "--hidden";
}
