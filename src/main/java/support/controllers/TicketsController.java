package support.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import support.model.Ticket;
import support.service.HelpService;
import support.ui.StageManager;
import support.ui.TicketListCell;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by togrul on 1/31/15.
 */
public class TicketsController implements Initializable {

    HelpService helpService;
    StageManager stageManager;

    @FXML
    ListView<Ticket> ticketListView;

    @FXML
    Button newTicketButton;

    ObservableList<Ticket> ticketListData = FXCollections.observableArrayList();

    public void newTicketButtonClick(ActionEvent actionEvent) {

        stageManager.openNewTicketStage();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        stageManager = new StageManager();
        helpService = HelpService.getInstance();

        ticketListView.setItems(ticketListData);

        ticketListView.setCellFactory(new Callback<ListView<Ticket>, ListCell<Ticket>>() {
            @Override
            public ListCell<Ticket> call(ListView<Ticket> param) {
                return new TicketListCell();
            }
        });

        try {
            List<Ticket> ticketList = helpService.getTickets();
            ticketListData.addAll(ticketList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ticketListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){

                        Ticket ticket = ticketListView.getSelectionModel().getSelectedItem();
                        stageManager.openTicketStage(ticket);

                    }
                }
            }
        });
    }

}
