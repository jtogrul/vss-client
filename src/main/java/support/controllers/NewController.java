package support.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;
import support.model.ApiResponse;
import support.model.Equipment;
import support.model.RequestType;
import support.model.Ticket;
import support.service.HelpService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by togrul on 2/1/15.
 */
public class NewController implements Initializable {

    HelpService helpService;

    @FXML
    TextField subjectTextField;
    @FXML
    TextArea messageTextArea;
    @FXML
    ComboBox<RequestType> requestTypeComboBox;
    @FXML
    ComboBox<Equipment> equipmentComboBox;
    @FXML
    ProgressIndicator saveProgressIndicator;
    @FXML
    GridPane formGridPane;
    @FXML
    Pane progressPane;
    @FXML
    Label errorLabel;
    @FXML
    Button sendButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        helpService = HelpService.getInstance();
        errorLabel.setVisible(false);

        startLoading();

        List<RequestType> requestTypeList = null;
        List<Equipment> equipmentList = null;

        try {
            requestTypeList = helpService.getRequestTypeList();
            equipmentList = helpService.getEquipmentList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ObservableList<RequestType> requestTypeObservableList = FXCollections.observableList(requestTypeList);
        ObservableList<Equipment> equipmentObservableList = FXCollections.observableList(equipmentList);

        requestTypeComboBox.setItems(requestTypeObservableList);
        equipmentComboBox.setItems(equipmentObservableList);

        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        sendButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.SEND));

        stopLoading();

    }

    public void handleSendNewTicketButtonClick(ActionEvent actionEvent) {

        startLoading();

        Ticket ticket = new Ticket();
        ticket.setSubject(subjectTextField.getText());
        ticket.setRequestType(requestTypeComboBox.getValue());
        ticket.setEquipment(equipmentComboBox.getValue());
        ticket.setFirstMessage(messageTextArea.getText());

        try {
            ApiResponse apiResponse = helpService.addTicket(ticket);

            if (apiResponse.isOK()) {
                Stage stage = (Stage) messageTextArea.getScene().getWindow();
                stage.close();
            } else {

                errorLabel.setText("Xəta baş verdi. Xahiş olunur bütün xanaları düzgün doldurun");
                errorLabel.setVisible(true);

            }

        } catch (Exception e) {
            e.printStackTrace();
            errorLabel.setText("Xəta baş verdi. Xahiş olunur bütün xanaları düzgün doldurun");
            errorLabel.setVisible(true);
        }

        stopLoading();
    }

    private void startLoading() {
        formGridPane.setEffect(new GaussianBlur());
        progressPane.setVisible(true);
    }

    private void stopLoading() {
        formGridPane.setEffect(null);
        progressPane.setVisible(false);
    }
}
