package support.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;
import support.model.ApiResponse;
import support.model.Message;
import support.model.Ticket;
import support.service.HelpService;
import support.ui.MessageListCell;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by togrul on 2/1/15.
 */
public class TicketController implements Initializable {

    HelpService helpService;
    Ticket ticket;

    @FXML
    Label subjectLabel;
    @FXML
    Label equipmentLabel;
    @FXML
    Label requestTypeLabel;
    @FXML
    Label statusLabel;
    @FXML
    TextArea messageTextArea;
    @FXML
    ListView<Message> messagesListView;
    @FXML
    SplitMenuButton openCloseButton;
    @FXML
    SplitMenuButton setSatisfactionButton;
    @FXML
    Button sendButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        helpService = HelpService.getInstance();

        messagesListView.setCellFactory(new Callback<ListView<Message>, ListCell<Message>>() {
            @Override
            public ListCell<Message> call(ListView<Message> param) {
                return new MessageListCell();
            }
        });

        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        sendButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.SEND));
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;

        // TODO set ticket as read

        loadDetailedTicketData();
    }

    public void loadDetailedTicketData() {

        try {
            ticket = helpService.getTicket(ticket.getId());

            subjectLabel.setText(ticket.getSubject());
            requestTypeLabel.setText(ticket.getRequestType().getName());
            equipmentLabel.setText(ticket.getEquipment().getName());
            statusLabel.setText(ticket.getStatus().getName());
            setSatisfactionButtonText(ticket.isSatisfied());
            setOpenCloseButtonText(ticket.isOpen());

            ObservableList<Message> messageObservableList =
                    FXCollections.observableList(ticket.getMessages());

            messagesListView.setItems(messageObservableList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOpenCloseButtonText(boolean open) {
        openCloseButton.setText(open ? "Açıq" : "Bağlı");
    }

    private void setSatisfactionButtonText(boolean satisfied) {
        setSatisfactionButton.setText(satisfied ? "Razı" : "Narazı");
    }

    public void handleSendMessageAction(ActionEvent actionEvent) {

        String messageBody = messageTextArea.getText();

        Message reply = new Message(messageBody, ticket);

        try {
            ApiResponse apiResponse = helpService.addMessage(reply);
            System.out.println(apiResponse);

            if (apiResponse.isOK()) {
                loadDetailedTicketData();
                messageTextArea.setText("");
            } else {
                System.out.println("error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void handleOpenTicketAction(ActionEvent actionEvent) {
        try {
            ApiResponse apiResponse = helpService.setOpen(ticket, true);
            System.out.println(apiResponse);
            loadDetailedTicketData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleCloseTicketAction(ActionEvent actionEvent) {
        try {
            ApiResponse apiResponse = helpService.setOpen(ticket, false);
            System.out.println(apiResponse);
            loadDetailedTicketData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleSetSatisfactionAction(ActionEvent actionEvent) {
        try {
            ApiResponse apiResponse = helpService.setSatisfaction(ticket, true);
            System.out.println(apiResponse);
            loadDetailedTicketData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleSetNotSatisfactionAction(ActionEvent actionEvent) {
        try {
            ApiResponse apiResponse = helpService.setSatisfaction(ticket, false);
            System.out.println(apiResponse);
            loadDetailedTicketData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
