package support.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import support.Main;
import support.config.Config;
import support.service.HelpService;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by togrul on 2/2/15.
 */
public class LoginController implements Initializable {

    HelpService helpService;
    Preferences preferences;

    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField passwordField;
    @FXML
    Pane progressPane;
    @FXML
    GridPane formGridPane;
    @FXML
    Label errorLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        helpService = HelpService.getInstance();
        preferences = Preferences.systemNodeForPackage(Main.class);

        errorLabel.setVisible(false);
        progressPane.setVisible(false);
    }

    public void handleLoginButtonClick(ActionEvent actionEvent) {
        attemptLogin();
    }

    private void attemptLogin() {

        String username = usernameTextField.getText();
        String password = passwordField.getText();

        startLoading();

        boolean success = false;

        try {
            success = helpService.checkAuth(username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (success) {
            preferences.put(Config.PREF_NAME_USER, username);
            preferences.put(Config.PREF_NAME_PASS, password);

            try {
                preferences.flush();
            } catch (BackingStoreException e) {
                e.printStackTrace();
            }

            Stage stage = (Stage) formGridPane.getScene().getWindow();
            stage.close();
        } else {

            stopLoading();
            errorLabel.setVisible(true);

        }

    }

    private void startLoading() {
        formGridPane.setEffect(new GaussianBlur());
        progressPane.setVisible(true);
    }

    private void stopLoading() {
        formGridPane.setEffect(null);
        progressPane.setVisible(false);
    }
}
