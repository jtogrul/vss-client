package support.controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;
import support.model.Message;
import support.service.HelpService;
import support.ui.StageManager;
import support.ui.UnreadMessageListCell;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    HelpService helpService;
    StageManager stageManager;

    @FXML
    private ListView<Message> unreadMessagesListView;
    @FXML
    private Label unreadMessagesInfoLabel;
    @FXML
    private Button newTicketButton;
    @FXML
    private Label userName;
    @FXML
    private Label userPosition;
    @FXML
    private ImageView userPhoto;

    public void handleNewTicketButtonClick(ActionEvent actionEvent) {

        stageManager.openNewTicketStage();

    }

    public void handleMyTicketsButtonClick(ActionEvent actionEvent) {

        stageManager.openMyTicketsStage();

    }

    public void handleRemoteControlButton(ActionEvent actionEvent) {

        String folder = System.getProperty("user.dir");
        String seperator = File.separator;
        try {
            Runtime.getRuntime().exec(folder + seperator + "Ammyy.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        helpService = HelpService.getInstance();
        stageManager = new StageManager();

        userName.setText(helpService.user.getFullName());
        userPosition.setText(helpService.user.getPosition());
        userPhoto.setImage(new Image("/icons/user.png"));


        unreadMessagesListView.setCellFactory(new Callback<ListView<Message>, ListCell<Message>>() {
            @Override
            public ListCell<Message> call(ListView<Message> param) {
                return new UnreadMessageListCell();
            }
        });
        unreadMessagesListView.setVisible(false);

        final ObservableList<Message> messageObservableList = helpService.getUnreadMessagesObservableList();

        messageObservableList.addListener(new ListChangeListener<Message>() {
            @Override
            public void onChanged(Change<? extends Message> c) {

                if (messageObservableList.size() > 0) {

                    unreadMessagesInfoLabel.setText(
                            String.format("Sizin %d oxunmamış mesajınız var:", messageObservableList.size())
                    );
                    unreadMessagesListView.setVisible(true);

                } else {

                    unreadMessagesInfoLabel.setText("Oxunmamış mesaj yoxdur");
                    unreadMessagesListView.setVisible(false);

                }

            }
        });

        unreadMessagesListView.setItems(messageObservableList);

        unreadMessagesListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){

                        Message message = unreadMessagesListView.getSelectionModel().getSelectedItem();
                        stageManager.openTicketStage(message.getTicket());

                    }
                }
            }
        });

        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        newTicketButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.PLUS));

    }
}
